
# XpatSolver

## Auteurs

* GARNIER Kevin, @garnierk, 22007979
* BORDAS Florent, @bordas, 22006083

## Fonctionnalités

XpatSolver gère actuellement la vérification d'un fichier solution bien
formaté pour FreeCell, Baker's Dozen, Midgnight Oil et Seahaven.

Il possède aussi une recherche de solutions non exhaustive avec la
possibilité d'écriture de la solution dans un fichier donné.

Ainsi que de trouver la solution d'un fichier non
complet au format XpatSolver.

## Compilation et exécution

Il est possible de compiler le projet en exécutant la commande `make` dans la
racine du dépôt.

On peut lancer le projet (et la compilation si elle n'est pas effectuée)
grâce à `./run game.seed [ARGS]` dans la racine du dépôt.

Les arguments valides sont les suivants:

### game.seed

`seed` est un entier compris entre 1 et 1 000 000 000.

* FreeCell : `Freecell.seed` ou `fc.seed`
* Baker's Dozen : `BakersDozen.seed` ou `bd.seed`
* Midgnight Oil : `MidnightOil.seed` ou `mo.seed`
* Seahaven : `Seahaven.seed` ou `st.seed`

### ARGS

`-check FILE` : Vérifie si le fichier FILE est bien solution de la partie.
Si le fichier est valide, le programme envoie `SUCCES` sinon il envoie
`ECHEC n` avec n la ligne de la première erreur.

`-search FILE` : Recherche une solution de la partie. Si elle existe, le
programme l'écrit sous le format XpatSolver et envoie `SUCCES` sur le terminal.
Sinon le fichier n'est pas modifié et `INSOLUBLE` est envoyé.

`-from FILE` : Reprend la solution partielle du fichier et essaie de la
compléter.

### Code de sortie

Si le programme n'a pas trouvé de solution, il écrit `INSOLUBLE` et quitte avec
le code d'erreur `2`.

Si le fichier envoyé à la vérification n'est pas solution de la partie, le
programme écrit `ECHEC n` avec n la ligne de la première erreur et quitte
avec le code d'erreur `1`.

Si une erreur a été rencontrée, le programme affiche l'erreur et quitte avec le
code d'erreur `1`.

Sinon, le programme écrit `SUCCES` et quitte avec le code d'erreur `0`.

## Modules

### Librairies génériques

#### `SortedList.ml`

Fournit une interface de listes triées afin d'alléger leurs utilisations pour les
registres. Elles gardent dans un enregistrement la fonction de comparaison
utilisée pour les créer.

#### `FStack.ml`

Fournit une interface purement fonctionnelle des piles. Elle est gérée en
interne par une liste et permet d'alléger son utilisation pour les colonnes
de cartes.

#### `Fifo.ml`

Fournit une interface fonctionnel d'une file persistante pour améliorer la complexité des opérations tels que pop et push en utilisant deux listes. Utilisé pour la génération de la permutation.

### Modules de représentations

#### `Card.ml`

Représente les cartes du jeu sous forme de couples `(rank, suit)` et gère les
conversions sous différent formats et la comparaison entre cartes

#### `Rules.ml`

Représente les règles d'une partie. Celles-ci sont encodées dans un
enregistrement comme le nombre de colonnes, le nombre de registres,
les conditions des cartes, etc.
Il y a aussi les fonctions de vérification de coups valides, c'est-à-dire si deux
cartes données peuvent se superposer ou si on peut la mettre sur une colonne
vide.

#### `State.ml`

Représente un état de la partie. Il est composé d'un registre, d'un dépôt,
du tableau des colonnes et de l'historique de coups joués représentée par une
liste de couples dont le premier élément est la carte à déplacer et le deuxième
la carte ou la colonne vide ou le registre où on essayera de déplacer la carte.

Il possède également une fonction d'évaluation dont le but est de qualifier la
qualité d'un état en comparant ses colonnes avec "la colonne parfaite",
c'est-à-dire la colonne constistuée de
`[As, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, Valet, Dame, Roi]`. Si ce n'est pas le
cas, en fonction de la capacité de la carte à bloquer la partie, on donne un malus
à l'état en plus du coût pour accéder à cette carte.

Il possède également une fonction de comparaison qui est chargée de détecter
les permutations de colonnes et donc de renvoyer `0` sinon on utilise la
comparaison standard d'Ocaml.

### Modules de calculs

#### `Play.ml`

Regroupe l'ensemble des fonctions de déroulement d'une partie, c'est-à-dire :

* la normalisation d'un état.

* La vérification si un coup donné est valide et renvoie le nouvel état avec le
coup joué si c'est le cas, sinon renvoie `None`.

* La vérification si un état est gagnant.

#### `Check.ml`

Vérifie si la solution donnée est valide en parcourant la liste de jetons,
calculée grâce au parser depuis le fichier à vérifier et en testant la validité des coups.

#### `Search.ml`

Réalise la recherche non exhaustive d'une solution.

On définit un seuil maximum du nombre d'états visités (100 000 par défaut).

On définit une fonction de contrôle qui est une suite `Un` définie telle que
si `Un > 0`, `Un+1 = Un * 0.5`
si `Un = 0`, `Un+1 = -1`
si `Un < 0`, `Un+1 = 2 * Un`
avec `U0 = 50`

On utilise le module Set pour éviter les redondances.

On ajoute à file d'état à calculer seulement ceux dont le score est un
intervalle `]-∞ ; father\_score + range]` avec `father\_score` le score
de l'état parent et `range` le résultat de notre fonction de contrôle de la
qualité des états.

Si le nombre d'états visités dépasse le seuil, on relance la recherche avec un
appel à la fonction de contrôle de qualité.

Si la file d'état est vide avant le dépassement du seuil, c'est que la partie
est insoluble.

#### `XpatRandom.ml`

Fournit l'implémentation de la génération de graine pour construire une partie de Xpat2.

#### `FileManipulation`

Fournit une implémentation pour lire les fichiers .sol et les transformer en notre type de donnée pour une carte et un coup, ainsi que la lecture de notre d'un State final pour l'écriture d'un fichier solution.

### Module principal

Tout d'abord on réalise la permutation des cartes, on construit nos règles selon
la partie demandée. Puis on construit l'état initial selon ces règles.
On le normalise et selon le type demandé, soit on lance la recherche
de solutions, soit on la lance la vérification.
On affiche, selon le retour, le code d'erreur ou de
réussite du programme.


### Remarque

Concernant la recherche de solutions, l'algorithme que nous avons implémenté n'est pas vraiment optimal pour certaines variantes (Freecell) bien qu'il arrive à trouver des ensembles solutions. Nous avons par la suite pensé à implémenter un algorithme A* adapté au cas du solitaire en créant une heuristique qui soit admissible pour trouver des solutions à peu près optimales. Cette heuristique aurait été basé sur le calcul d'un score en fonction du score de dépôt de l'état des colonnes et de l'état des registres ainsi que le nombre de coups à jouer approximatif pour atteindre l'état final. En affectant à chaque heuristique un certain coefficients selon la règle
en expérimentant, la fonction de recherche aurait pu trouver des solutions beaucoup plus satisfaisantes.
