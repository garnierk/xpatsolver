val read_file : string -> Stdlib.in_channel
val read_line : Stdlib.in_channel -> string
val convert_to_type : string -> State.play
val parse_components : string list -> State.play -> State.play * State.play
val init_parse : string -> (State.play * State.play) list
val write_open : string -> out_channel
val sol_file : State.t -> string -> unit
