open Rules
open Play
open State

module SSet = Set.Make (struct
  type t = State.t

  let compare s1 s2 = State.compare s1 s2
end)

let threshold = 10_000
let default_range = 10

let drange range =
  if range > 1 then range / 2
  else if range = 1 || range = 0 then -2
  else 2 * range

let compute rules state p = normalize rules (Option.get (play rules state p))

let add_state range father_score rules state p checked plays =
  let next = compute rules state p in
  let score = State.evaluate rules next in
  if score - father_score > range || SSet.mem next checked then (plays, checked)
  else (next :: plays, SSet.add next checked)

(* Check the possibles plays from the given card*)
let check_possible_cols range father_score checked rules state cols card plays
    empty =
  let rec loop checked plays i =
    if i >= rules.cols_n then (plays, checked)
    else
      let stack = FArray.get cols i in
      let c, _ = FStack.pop_option stack in
      match c with
      | None ->
          if check_empty rules card && not empty then
            let card_in = { card = Some card; card_type = CARD }
            and card_out = { card = None; card_type = EMPTY } in
            let plays, checked =
              add_state range father_score rules state (card_in, card_out)
                checked plays
            in
            loop checked plays (i + 1)
          else loop checked plays (i + 1)
      | Some card_at ->
          if check_non_empty rules card card_at then
            let card_in = { card = Some card; card_type = CARD }
            and card_out = { card = Some card_at; card_type = CARD } in
            let plays, checked =
              add_state range father_score rules state (card_in, card_out)
                checked plays
            in
            loop checked plays (i + 1)
          else loop checked plays (i + 1)
  in
  loop checked plays 0

let check_possible_reg range father_score checked rules state cols
    (reg : Card.card SortedList.t) plays =
  let rec loop checked l plays =
    match l with
    | [] -> (plays, checked)
    | card :: l ->
        let plays, checked =
          check_possible_cols range father_score checked rules state cols card
            plays false
        in
        loop checked l plays
  in
  loop checked reg.content plays

let possible_plays range father_score checked rules state =
  let rec aux checked rules state cols len_reg plays i =
    if i >= rules.cols_n then (plays, checked)
    else
      let stack = FArray.get cols i in
      let card, s = FStack.pop_option stack in
      match card with
      | None -> aux checked rules state cols len_reg plays (i + 1)
      | Some card ->
          let plays, checked =
            if len_reg < rules.reg_n then
              let card_in = { card = Some card; card_type = CARD }
              and card_out = { card = None; card_type = REG } in
              add_state range father_score rules state (card_in, card_out)
                checked plays
            else (plays, checked)
          in
          let plays, checked =
            check_possible_cols range father_score checked rules state cols card
              plays (FStack.empty = s)
          in
          aux checked rules state cols len_reg plays (i + 1)
  in

  let plays, checked =
    aux checked rules state state.cols (SortedList.length state.reg) [] 0
  in
  check_possible_reg range father_score checked rules state state.cols state.reg
    plays

let rec search_under_threshold range father_score rules push_list pop queue
    checked n =
  if n > threshold then (None, -1)
  else
    let state, queue = pop queue in
    match state with
    | None -> (None, n)
    | Some state -> (
        if is_winning rules state then (Some state, n)
        else
          let plays, checked =
            possible_plays range father_score checked rules state
          in
          let father_score = State.evaluate rules state in
          match plays with
          | [] ->
              search_under_threshold range father_score rules push_list pop
                queue checked (n + 1)
          | _ ->
              search_under_threshold range father_score rules push_list pop
                (push_list plays queue) checked (n + 1))

let search father_score rules push_list pop queue checked =
  let rec aux range father_score rules push_list pop queue checked =
    Printf.printf "Searching with range %d...\n" range;
    flush stdout;
    let state, n =
      search_under_threshold range father_score rules push_list pop queue
        checked 0
    in
    if n = -1 then (
      Printf.printf
        "No solutions found below the threshold %d, trying with lower range \n\n"
        threshold;
      flush stdout;
      aux (drange range) father_score rules push_list pop queue checked)
    else (state, n)
  in
  aux default_range father_score rules push_list pop queue checked

let dfs rules state =
  let state, n =
    search
      (State.evaluate rules state)
      rules FStack.push_list FStack.pop_option (FStack.of_list [ state ])
      (SSet.singleton state)
  in
  match state with
  | None -> (None, n)
  | Some state ->
      (*List.iter print_play (FStack.to_list state.plays);*)
      (Some state, n)
