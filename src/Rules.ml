(** Game rules **)

type color = Alt | Same | All | SameSuit
type order = Upper | Under
type cardspec = All | King | CNone

type t = {
  reg_n : int;
  cols_n : int;
  cards_by_cols : int list;
  non_empty : order * color;
  empty : cardspec;
  cols_back : cardspec;
  depos_n : int;
}

let make reg_n cols_n cards_by_cols non_empty empty cols_back depos_n =
  if List.length cards_by_cols != cols_n then
    raise (Invalid_argument "cols_n and cards_by_cols length are differents")
  else { reg_n; cols_n; cards_by_cols; non_empty; empty; cols_back; depos_n }

let is_black card =
  let _, suit = card in
  suit = Card.Trefle || suit = Card.Pique

let is_red card =
  let _, suit = card in
  suit = Card.Carreau || suit = Card.Coeur

let check_color color card card_at =
  let _, a = card and __, b = card_at in
  match color with
  | Alt -> (is_red card && is_black card_at) || (is_black card && is_red card_at)
  | Same ->
      (is_red card && is_red card_at) || (is_black card && is_black card_at)
  | All -> true
  | SameSuit -> a = b

let check_order order card card_at =
  let a, _ = card and b, _ = card_at in
  match order with Upper -> a = b + 1 | Under -> a + 1 = b

let check_cardspec spec card = false

let check_non_empty rules card card_at =
  let order, color = rules.non_empty in
  check_color color card card_at && check_order order card card_at

let check_empty rules (card : Card.card) =
  let rank, _ = card in
  match rules.empty with
  | All -> true
  | King -> rank = Card.king
  | CNone -> false
