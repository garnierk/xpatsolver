open Rules
(**State*)

type card_type = EMPTY | CARD | REG
type play = { card : Card.card option; card_type : card_type }

type t = {
  (*config : int list;*)
  depos : int FArray.t;
  cols : Card.card FStack.t FArray.t;
  reg : Card.card SortedList.t;
  plays : (play * play) FStack.t;
}

let make depos cols reg plays = { depos; cols; reg; plays }

let array_to_string content_format array_format array =
  let rec aux content_format array_format array result i size =
    if i >= size then result
    else
      aux content_format array_format array
        (array_format result (content_format (FArray.get array i)))
        (i + 1) size
  in
  aux content_format array_format array
    (content_format (FArray.get array 0))
    1 (FArray.length array)

let cols_to_string cols =
  let cat a b = a ^ "\n" ^ b in
  array_to_string (FStack.to_string Card.to_string) cat cols

let depos_to_string depos =
  let cat a b = a ^ "; " ^ b in
  "[" ^ array_to_string string_of_int cat depos ^ "]"

let to_string state =
  "depository: "
  ^ depos_to_string state.depos
  ^ "\nregistry: "
  ^ SortedList.to_string Card.to_string state.reg
  ^ "\ncolumns:\n" ^ cols_to_string state.cols ^ "\n"

let add_play play state =
  let plays = FStack.push play state.plays in
  make state.depos state.cols state.reg plays

let is_good rules card last =
  let r, _ = card in
  if r = Card.king then false
  else
    match last with
    | None -> true
    | Some last -> check_non_empty rules last card

let malus rules (r, s) last cols depos =
  let next = FArray.get depos (Card.num_of_suit s) + 1 in
  if r = Card.king then
    let is_empty stack = FStack.compare Card.compare FStack.empty stack = 0 in
    if FArray.exists is_empty cols && last = None && rules.empty = CNone then
      (1, (0, Card.Coeur))
    else (13, (0, Card.Coeur))
  else if r = next then (13, (r, s))
  else
    let lr, ls = Option.get last in
    (abs (lr - r), (r, s))

let evaluate_stack rules depos stack cols =
  let rec aux rules depos stack cols last score i =
    try
      let card, stack = FStack.pop stack in
      if is_good rules card last then
        aux rules depos stack cols (Some card) (score + i) (i + 1)
      else
        let m, n = malus rules card last cols depos in
        aux rules depos stack cols (Some n) (score + m + i) (i + 1)
    with _ -> score
  in
  aux rules depos stack cols None 0 0

let evaluate_reg (reg : Card.card SortedList.t) =
  let rec aux l score =
    match l with [] -> score | (r, s) :: l -> aux l (score + r)
  in
  aux reg.content 0
let evaluate_depos depos len =
  let rec aux i score =
    if i >= len then score
    else aux (i + 1) (score + (FArray.get depos i) )
  in (52 - (aux 0 0)) * 5

let evaluate rules state =
  let rec aux rules state i len cols score =
    if i >= len then score
    else
      aux rules state (i + 1) len cols
        (score + evaluate_stack rules state.depos (FArray.get state.cols i) cols)
  in
  let score = evaluate_reg state.reg in
  score + aux rules state 0 rules.cols_n state.cols 0 + evaluate_depos state.depos rules.depos_n

let rec search_stack s1 cols2 seen i len =
  if i >= len then (-1, -1)
  else
    let s2 = FArray.get cols2 i in
    let c = FStack.compare Card.compare s1 s2 in
    if c = 0 && not (SortedList.exists i seen) then (i, 0)
    else search_stack s1 cols2 seen (i + 1) len

let compare_cols cols1 cols2 seen i len =
  let rec aux cols1 cols2 seen i len =
    if i >= len then 0
    else
      let s = FArray.get cols1 i in
      let index, b = search_stack s cols2 seen 0 len in
      if b <> 0 then b
      else aux cols1 cols2 (SortedList.insert index seen) (i + 1) len
  in
  aux cols1 cols2 seen i len

let ord rules s1 s2 =
  let c = Int.compare (evaluate rules s1) (evaluate rules s2) in
  if c <> 0 then c
  else
    let c = FArray.compare Int.compare s1.depos s2.depos in
    if c <> 0 then c else 0

let equals s1 s2 =
  let c = SortedList.compare s1.reg s2.reg in
  if c <> 0 then false
  else
    let seen = SortedList.make Int.compare [] in
    compare_cols s1.cols s2.cols seen 0 (FArray.length s1.cols) = 0

let compare s1 s2 = if equals s1 s2 then 0 else Stdlib.compare s1 s2

let play_to_string (card_in, card_out) =
  let cin = Int.to_string (Card.to_num (Option.get card_in.card))
  and out =
    match card_out.card_type with
    | EMPTY -> "V"
    | REG -> "T"
    | CARD -> Int.to_string (Card.to_num (Option.get card_out.card))
  in
  cin ^ " " ^ out

let plays_to_string state =
  let rec loop buf = function
    | [] -> buf
    | p :: l -> loop (buf ^ play_to_string p ^ "\n") l
  in
  loop "" (FStack.to_list state.plays)
