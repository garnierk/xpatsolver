(* Check if the list of plays leads to a final state*)
val check :
  Rules.t -> State.t -> (State.play * State.play) list -> State.t option * int

(* Check if the list of plays leads to a final state, if not return the last good state*)
val partial_check :
  Rules.t -> State.t -> (State.play * State.play) list -> State.t
