open Play
open State
open Rules

let check rules state tokens =
  let rec aux rules state n tokens =
    match tokens with
    | [] -> if is_winning rules state then (Some state, n) else (None, n)
    | next :: cards' -> (
        match (play rules state next) with
        | None -> (None, n)
        | Some new_state ->
            let state = normalize rules new_state in
            if is_winning rules state then (Some state, n)
            else aux rules state (n + 1) cards')
  in
  aux rules state 1 tokens

  let partial_check rules state tokens =
    let rec aux rules state n tokens =
      match tokens with
      | [] -> if is_winning rules state then state else state
      | next :: cards' -> (
          match (play rules state next) with
          | None -> state
          | Some new_state ->
              let state = normalize rules new_state in
              if is_winning rules state then new_state
              else aux rules state (n + 1) cards')
    in
    aux rules state 1 tokens
