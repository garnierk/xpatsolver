(**State*)

type card_type = EMPTY | CARD | REG
type play = { card : Card.card option; card_type : card_type }

type t = {
  (*config : int list;*)
  depos : int FArray.t;
  cols : Card.card FStack.t FArray.t;
  reg : Card.card SortedList.t;
  plays : (play * play) FStack.t;
}

val make :
  int FArray.t ->
  Card.card FStack.t FArray.t ->
  Card.card SortedList.t ->
  (play * play) FStack.t ->
  t

val to_string : t -> string

val compare : t -> t -> int
val add_play : play * play -> t -> t
val evaluate : Rules.t -> t -> int
val plays_to_string : t -> string
