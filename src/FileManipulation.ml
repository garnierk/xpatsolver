(*Interface for the manipulation of solution files, 
reading and translating into possible plays and on the 
opposite translating a play list into a solution file *)


open Card
open State

let read_file filename = try open_in filename with e -> raise e

let read_line channel =
  try input_line channel
  with e ->
    close_in_noerr channel;
    raise e

let convert_to_type s : play =
  match s with
  | "T" -> { card = None; card_type = REG }
  | "V" -> { card = None; card_type = EMPTY }
  | s -> { card = Some (Card.of_num (int_of_string s)); card_type = CARD }

let rec parse_components components card_struct : play * play =
  match components with
  | [ x ] -> (card_struct, convert_to_type x)
  | h :: t -> parse_components t (convert_to_type h)
  | [] -> assert false

let card = { card = None; card_type = CARD }

let init_parse filename =
  let channel = read_file filename in
  let rec parse_file channel list =
    try
      let line = read_line channel in
      let string_list = String.split_on_char ' ' line in
      let struct_tuple = parse_components string_list card in
      parse_file channel (struct_tuple :: list)
    with e -> if e = End_of_file then list else raise e
  in

  List.rev (parse_file channel [])


let write_open filename =
  try open_out filename
  with e -> raise e
let sol_file p_list filename =
  let o_channel = write_open filename in
  let s_list = plays_to_string p_list in
  Printf.fprintf o_channel "%s" s_list
