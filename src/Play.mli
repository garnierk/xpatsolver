(*Compute the deplacment of cards*)

(* Normalize a state by putting the cards in the depositry*)
val normalize : Rules.t -> State.t -> State.t
val play : Rules.t -> State.t -> State.play * State.play -> State.t option
val is_winning : Rules.t -> State.t -> bool
