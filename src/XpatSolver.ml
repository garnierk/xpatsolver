open XpatLib

type game = Freecell | Seahaven | Midnight | Baker
type exit_code = Succes | Fail of int | Insoluble
type from = string

type mode =
  | Check of string (* filename of a solution file to check *)
  | Search of string (* filename where to write the solution *)

type config = {
  mutable game : game;
  mutable seed : int;
  mutable mode : mode;
  mutable from : from;
}

let config =
  { game = Freecell; seed = 1; mode = Search ""; from = "" }

let getgame = function
  | "FreeCell" | "fc" -> Freecell
  | "Seahaven" | "st" -> Seahaven
  | "MidnightOil" | "mo" -> Midnight
  | "BakersDozen" | "bd" -> Baker
  | _ -> raise Not_found

let split_on_dot name =
  match String.split_on_char '.' name with
  | [ string1; string2 ] -> (string1, string2)
  | _ -> raise Not_found

let set_game_seed name =
  try
    let sname, snum = split_on_dot name in
    config.game <- getgame sname;
    config.seed <- int_of_string snum
  with _ ->
    failwith
      ("Error: <game>.<number> expected, with <game> in "
     ^ "FreeCell Seahaven MidnightOil BakersDozen")

let make_rules config =
  match config.game with
  | Freecell ->
      Rules.make 4 8 [ 7; 6; 7; 6; 7; 6; 7; 6 ] (Rules.Under, Rules.Alt)
        Rules.All Rules.All 4
  | Seahaven ->
      Rules.make 4 10
        [ 5; 5; 5; 5; 5; 5; 5; 5; 5; 5 ]
        (Rules.Under, Rules.SameSuit)
        Rules.King Rules.All 4
  | Midnight ->
      Rules.make 0 18
        [ 3; 3; 3; 3; 3; 3; 3; 3; 3; 3; 3; 3; 3; 3; 3; 3; 3; 1 ]
        (Rules.Under, Rules.SameSuit)
        Rules.CNone Rules.All 4
  | Baker ->
      Rules.make 0 13
        [ 4; 4; 4; 4; 4; 4; 4; 4; 4; 4; 4; 4; 4 ]
        (Rules.Under, Rules.All) Rules.CNone Rules.King 4

let add_card (rules : Rules.t) permut stack size =
  let rec aux (rules : Rules.t) permut stack i size =
    if i >= size then (stack, permut)
    else
      match permut with
      | [] -> (stack, [])
      | c :: permut ->
          let r, s = Card.of_num c in
          let stack =
            if rules.cols_back = Rules.King && r = 13 (* if the card is a king*)
            then FStack.push_front (Card.of_num c) stack
            else FStack.push (Card.of_num c) stack
          in
          aux rules permut stack (i + 1) size
  in
  aux rules permut stack 0 size

let init_cards (rules : Rules.t) permut =
  let rec aux (rules : Rules.t) sizes permut cols reg i =
    match sizes with
    | [] -> (cols, SortedList.append (List.rev_map Card.of_num permut) reg)
    | s :: sizes ->
        let stack, permut = add_card rules permut (FArray.get cols i) s in
        aux rules sizes permut (FArray.set cols i stack) reg (i + 1)
  in
  let cols = FArray.make rules.cols_n FStack.empty in
  let reg = SortedList.make Card.compare [] in
  aux rules rules.cards_by_cols permut cols reg 0

let init_game (rules : Rules.t) permut =
  let cols, reg = init_cards rules permut and depos = FArray.make 4 0 in
  State.make depos cols reg FStack.empty

let check rules state tokens =
  match Check.check rules state tokens with
  | None, n -> Fail n
  | Some state, n -> Succes

let search rules state file_out =
  match Search.dfs rules state with
  | None, n -> Insoluble
  | Some state, n ->
      FileManipulation.sol_file state file_out;
      Succes

let exit_with = function
  | Succes ->
      print_string "\nSUCCES\n";
      exit 0
  | Fail n ->
      Printf.printf "\nECHEC %d\n" n;
      exit 1
  | Insoluble ->
      Printf.printf "\nINSOLUBLE\n";
      exit 2


let init_from rules state =
  if config.from <> "" then (
    let state = Check.partial_check rules state (FileManipulation.init_parse config.from) in
    Printf.printf "\nPartial state:\n%s\n" (State.to_string state);
    flush stdout)
  else Printf.printf "\nInitial state:\n%s\n" (State.to_string state);
  flush stdout;
  state

let exec rules state =
  match config.mode with
  | Search file_out -> exit_with (search rules (init_from rules state) file_out)
  | Check file_in -> exit_with (check rules state (FileManipulation.init_parse file_in))

let treat_game conf =
  let permut = XpatRandom.shuffle conf.seed in
  Printf.printf "\nVoici juste la permutation de graine %d:\n" conf.seed;
  List.iter
    (fun n ->
      print_int n;
      print_string " ")
    permut;
  print_newline ();
  List.iter
    (fun n -> Printf.printf "%s " (Card.to_string (Card.of_num n)))
    permut;
  print_newline ();
  let rules = make_rules conf in
  let state = init_game rules permut in
  let state = Play.normalize rules state in
  exec rules state

let convert_xpat2 = function
  | "f" -> false
  | "false" -> false
  | "t" -> true
  | "true" -> true
  | _ -> false

let main () =
  Arg.parse
    [
      ( "-check",
        String (fun filename -> config.mode <- Check filename),
        "<filename>:\tValidate a solution file" );
      ( "-search",
        String (fun filename -> config.mode <- Search filename),
        "<filename>:\tSearch a solution and write it to a solution file" );
      ( "-from",
        String (fun filename -> config.from <- filename),
        "<filename>:\tCheck if the file is valid, if not search the solution\n\
        \          from the last valid state." );
    ]
    set_game_seed (* pour les arguments seuls, sans option devant *)
    "XpatSolver <game>.<number> : search solution for Xpat2 game <number>";
  if config.seed >= XpatRandom.randmax then
    failwith
      ("Error : Seed generation is greater than the maximal value which is "
      ^ string_of_int XpatRandom.randmax)
  else treat_game config

let _ = if not !Sys.interactive then main () else ()
