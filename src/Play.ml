open Rules
open State
open Card
open FStack
open FArray
open SortedList

let add_depos card depos =
  let rank, suit = card in
  let ind = num_of_suit suit in
  let last_rank = get depos ind in
  if last_rank <= king && rank == last_rank + 1 then (true, set depos ind rank)
  else (false, depos)

let normalize_cols rules cols depos =
  let rec aux cols depos i b len =
    if i >= len then (b, cols, depos)
    else
      let stack = get cols i in
      try
        let card, stack' = pop stack in
        let added, depos = add_depos card depos in
        let stack = if added then stack' else stack in
        aux (set cols i stack) depos (i + 1) (b || added) len
      with _ -> aux cols depos (i + 1) b len
  in
  aux cols depos 0 false rules.cols_n

let normalize_reg reg depos =
  let rec aux l depos b acc =
    match l with
    | [] -> (b, List.rev acc, depos)
    | card :: l' ->
        let added, depos = add_depos card depos in
        if added then aux l' depos added acc else aux l' depos b (card :: acc)
  in
  let added, content, depos = aux reg.content depos false [] in
  let reg = SortedList.make reg.cmp content in
  (added, reg, depos)

let rec normalize rules state =
  let modified, reg, depos =
    if not (rules.reg_n = 0) then normalize_reg state.reg state.depos
    else (false, state.reg, state.depos)
  in
  let added, cols, depos = normalize_cols rules state.cols depos in
  let state = State.make depos cols reg state.plays in
  if (not added) && not modified then state else normalize rules state

let loop_cols f cols card len =
  let rec aux f cols card len i b =
    if i >= len then (b, cols)
    else
      let stack = get cols i in
      let modified, stack' = f card stack in
      if modified then (true, set cols i stack')
      else aux f cols card len (i + 1) false
  in
  aux f cols card len 0 false

let check_card card stack =
  try
    let x, stack' = pop stack in
    let a, b = x and c, d = card in
    if x = card then (true, stack') else (false, stack)
  with _ -> (false, stack)

let modifies_cols f rules state card =
  let modified, cols = loop_cols f state.cols card rules.cols_n in
  if not modified then None
  else Some ({state with cols = cols})

let remove_card rules state card =
  if rules.reg_n != 0 && SortedList.exists card state.reg then
    Some ( {state with reg = (SortedList.remove card state.reg) })
  else modifies_cols check_card rules state card

let add_next_empty rules state card =
  if not (check_empty rules card) then None
  else
    let add_empty card stack =
      if FStack.empty = stack then (true, FStack.push card stack)
      else (false, stack)
    in
    modifies_cols add_empty rules state card

let add_at rules state card_at card_in =
  if not (check_non_empty rules card_in card_at) then None
  else
    let aux card_at card stack =
      let b, _ = check_card card_at stack in
      if b then (true, FStack.push card stack) else (false, stack)
    in
    modifies_cols (aux card_at) rules state card_in

let add_reg rules state card =
  if SortedList.length state.reg >= rules.reg_n then None
  else Some ({state with reg = (SortedList.insert card state.reg)})

let play rules state (card_in, card_out) =
  let state = (remove_card rules state (Option.get card_in.card)) in
  match state with
  | None -> None
  | Some state -> (
      let state = add_play (card_in, card_out) state in
      match card_out.card_type with
      | EMPTY -> add_next_empty rules state (Option.get card_in.card)
      | CARD ->
          add_at rules state (Option.get card_out.card)
            (Option.get card_in.card)
      | REG -> add_reg rules state (Option.get card_in.card))

let is_winning rules state =
  let rec aux depos b i len =
    if i >= len then b
    else aux depos (b && FArray.get depos i == Card.king) (i + 1) len
  in
  aux state.depos true 0 rules.depos_n

let check_possible_cols rules cols card plays =
  let rec aux rules cols card plays i =
    if i >= rules.cols_n then plays
    else
      let stack = get cols i in
      try
        let card_at, _ = pop stack in
        if check_non_empty rules card card_at then
          let card_in = { card = Some card; card_type = CARD }
          and card_out = { card = Some card_at; card_type = CARD } in
          aux rules cols card ((card_in, card_out) :: plays) (i + 1)
        else aux rules cols card plays (i + 1)
      with _ ->
        if check_empty rules card then
          let card_in = { card = Some card; card_type = CARD }
          and card_out = { card = None; card_type = EMPTY } in
          aux rules cols card ((card_in, card_out) :: plays) (i + 1)
        else aux rules cols card plays (i + 1)
  in
  aux rules cols card plays 0

let check_possible_reg rules cols reg plays =
  let rec aux rules cols l plays =
    match l with
    | [] -> plays
    | card::l -> aux rules cols l (check_possible_cols rules cols card plays)
  in aux rules cols reg.content plays

let possible_plays rules state =
  let rec aux rules cols len_reg plays i =
    if i >= rules.cols_n then plays
    else
      let stack = get cols i in
      try
        let card, _ = pop stack in
        let plays =
          if len_reg < rules.reg_n then
            let card_in = { card = Some card; card_type = CARD }
            and card_out = { card = None; card_type = REG } in
            (card_in, card_out) :: plays
          else plays
        in
        aux rules cols len_reg
          (check_possible_cols rules cols card plays)
          (i + 1)
      with _ -> aux rules cols len_reg plays (i + 1)
  in
  let plays = aux rules state.cols (length state.reg) [] 0 in
  check_possible_reg rules state.cols state.reg plays
