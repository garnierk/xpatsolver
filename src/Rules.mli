(** Game rules *)

(** Indicates the color of the next added card*)
type color = Alt | Same | All | SameSuit

(** Indicates the order of the next added card*)
type order = Upper | Under

(** Indicates some specific rules for the cards*)
type cardspec = All | King | CNone

type t = {
  reg_n : int;
  cols_n : int;
  cards_by_cols : int list;
  non_empty : order * color;
  empty : cardspec;
  cols_back : cardspec;
  depos_n : int;
}
(** Abstract game rules*)

val make :
  int -> int -> int list -> order * color -> cardspec -> cardspec -> int -> t
(** Makes the rules from its arguments *)

(* Check the movement of the given card to a pile where the last card is the second *)
val check_non_empty : t -> Card.card -> Card.card -> bool

(* Check the movement of the given card to an empty stack *)
val check_empty : t -> Card.card -> bool
