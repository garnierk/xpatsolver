type 'a t = 'a list * 'a list (* head of list = first out *)

let empty : 'a t = ([], [])
let is_empty = function 
| [], [] -> true 
| _ -> false
let push x (l1, l2) = (l1, x::l2)

let pop (l1,l2) = 
  match (l1,l2) with 
  | [], [] -> invalid_arg "pop"
  | x :: l1, l2 -> x, (l1,l2)
  | [], l2 -> match List.rev l2 with
              | x::l1 -> x, (l1, [])
              | [] -> assert false
  
let rec push_list l (l1, l2) = 
  match l with
  | [] -> (l1,l2)
  | x::t -> push_list t (push x (l1,l2))

let pop_option (l1,l2) = 
  match (l1,l2) with 
  | [], [] -> invalid_arg "pop"
  | x :: l1, l2 -> Some x, (l1,l2)
  | [], l2 -> match List.rev l2 with
              | x::l1 -> None, (l1, [])
              | [] -> assert false
let of_list l = push_list l ([], []) 
let to_list (l1, l2) = l1 @ List.rev l2
