binary:
	dune build src/XpatSolver.exe

byte:
	dune build src/XpatSolver.bc

clean:
	dune clean
	rm -f ./XpatSolver.bc

test:
	dune runtest

debug: byte
	rm -f ./XpatSolver.bc
	cp _build/default/src/XpatSolver.bc .
	ocamldebug XpatSolver.bc


run: binary
	./run
summary:
	@./test-summary
